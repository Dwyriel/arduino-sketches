#ifndef FUNCTIONS_HEADER
#define FUNCTIONS_HEADER

#include "includes.h"
#include "defines.h"
#include "structs.h"
#include "variables.h"

void checkButtonPress() {
    uint16_t readValue = analogRead(pinButtons);
    for (uint8_t i = 0; i < buttonsSize; ++i) //looping through all of them all the time to make sure we know which button is pressed and easily set if a button was "just pressed", slower than manually doing it with "if else"s but better to work with.
        buttons[i]->checkIfButtonPressed(readValue);
}

void changeMenu(MenuTemplate *nextMenu) {
    currentMenu->exit();
    currentMenu = nextMenu;
    currentMenu->enter();
}

uint8_t numFromHexDigit(char hex) {
    for (uint8_t i = 0; i < 16; i++)
        if (hexDigits[i] == hex)
            return i;
    return 0xFF;
}

arrayChar16 int16StringRep(uint16_t num) {
    arrayChar16 arr;
    for (uint8_t i = 0; i < INT16_NUM_BITS; ++i) 
        arr[i] = binaryDigits[(num >> (INT16_NUM_BITS - 1 - i)) & 0b1]; 
    return arr;
} 
 
void writeLineToRow(const char *line, uint8_t row = 0) {
    lcd.setCursor(0, row);
    uint8_t leftover = lcdCols;
    for (uint8_t i = 0; i < (line != nullptr ? lcdCols : 0); ++i) {
        if (line[i] == '\0') {
            leftover = lcdCols - i;
            break;
        }
        lcd.write(line[i]);
    }
    for (uint8_t i = 0; i < leftover; ++i)
        lcd.write(' ');
}

void writeLineToRowNoClear(const char *line, uint8_t row = 0) {
    if (line == nullptr)
        return;
    lcd.setCursor(0, row);
    for (uint8_t i = 0; i < lcdCols; ++i) {
        if (line[i] == '\0')
            return;
        lcd.write(line[i]);
    }
}

void writeLineToRowRight(const char *line, uint8_t row = 0) {
    if (line == nullptr) 
        return writeLineToRow(line, row);
    lcd.rightToLeft();
    uint8_t lineSize = lcdCols;
    uint8_t leftover = 0;
    lcd.setCursor(lcdCols - 1, row);
    for (uint8_t i = 0; i < lcdCols; ++i)
        if (line[i] == '\0') {
            lineSize = i;
            leftover = lcdCols - i;
            break;
        }
    for (uint8_t i = lineSize - 1; i != 0xFF; --i)
        lcd.write(line[i]);
    for (uint8_t i = 0; i < leftover; ++i)
        lcd.write(' ');
    lcd.leftToRight();
}

void writeLineToRowRightNoClear(const char *line, uint8_t row = 0) {
    if (line == nullptr)
        return;
    lcd.rightToLeft();
    uint8_t lineSize = lcdCols;
    lcd.setCursor(lcdCols - 1, row);
    for (uint8_t i = 0; i < lcdCols; ++i)
        if (line[i] == '\0') {
            lineSize = i;
            break;
        }
    for (uint8_t i = lineSize - 1; i != 0xFF; --i)
        lcd.write(line[i]);
    lcd.leftToRight();
}

/* Constructors */
MenuTemplate::MenuTemplate(const char *title, MenuTemplate *previousMenu = nullptr) : title(title), previousMenu(previousMenu) {};

MainMenu::MainMenu(const char *title, MenuTemplate *previousMenu = nullptr) : MenuTemplate(title, previousMenu) {};

JumpTheRock::JumpTheRock(const char *title, MenuTemplate *previousMenu = nullptr) : MenuTemplate(title, previousMenu) {};

GuessTheNumber::GuessTheNumber(const char *title, MenuTemplate *previousMenu = nullptr) : MenuTemplate(title, previousMenu) {};

/* MainMenu */
void MainMenu::enter() {
    if (justStarted) {
        justStarted = false;
        writeLineToRow("Welcome to the");
        writeLineToRowRight(title, 1);
        delay(2000);
    }
    showOption();
}

void MainMenu::loop() {
    if (btnLeft.justClicked) {
        this->exit();
        this->enter();
        return;
    }
    if (btnDown.justClicked) {
        if (++currentOption == subMenusSize)
            currentOption = 0;
        showOption();
    }
    if (btnUp.justClicked) {
        if (--currentOption == 0xFF)
            currentOption = subMenusSize - 1;
        showOption();
    }
    if (btnRight.justClicked || btnSelect.justClicked) {
        if (currentOption >= subMenusSize && currentOption <= 0xFF) {
            writeLineToRow("Not implemented");
            writeLineToRow(nullptr, 1);
            return;
        }
        changeMenu(subMenus[currentOption]);
    }
}

void MainMenu::exit() {}

void MainMenu::showOption() {
    writeLineToRow("Select option:");
    writeLineToRowRight(subMenus[currentOption]->title, 1);
}

/* GuessTheNumber */
void GuessTheNumber::enter() {
    generateNumber();
    resetGuess();
    writeLineToRow("Take a guess :)");
    printGuess();
    lcd.setCursor(currentDigit, 1);
    lcd.blink();
}

void GuessTheNumber::loop() {
    static unsigned long leftClickTime = 0;
    if (btnLeft.justClicked) {
        leftClickTime = millis();
        return;
    }
    if (btnLeft.btnCurrentState && millis() - leftClickTime > 1500) {
        changeMenu(previousMenu);
        return;
    }
    if (btnLeft.justReleased) {
        if (--currentDigit == 0xFF)
            currentDigit = numOfDigits - 1;
        lcd.setCursor(currentDigit, 1);
    }
    if (btnRight.justClicked) {
        if (++currentDigit == numOfDigits)
            currentDigit = 0;
        lcd.setCursor(currentDigit, 1);
    }
    if (btnUp.justClicked) {
        if (++currentGuess[currentDigit] == 10) 
            currentGuess[currentDigit] = 0;
        printGuess();
    }
    if (btnDown.justClicked) {
        if (--currentGuess[currentDigit] == -1) 
            currentGuess[currentDigit] = 9;
        printGuess();
    }
    if (btnSelect.justClicked){
        ++numOfGuesses;
        swapPhrase = !swapPhrase;
        switch (checkCurrentGuess()) {
            case 1:
                writeLineToRow(swapPhrase ? "Number too big" : "Too big");
                break;
            case -1:
                writeLineToRow(swapPhrase ? "Number too small" : "Too small");
                break;
            case 0:
                lcd.noBlink();
                writeLineToRow("You got it right");
                writeLineToRow("Tries:", 1);
                char buffer[6] = {0};
                itoa(numOfGuesses, buffer, 10);
                writeLineToRowRightNoClear(buffer, 1);
                delay(5000);
                ++numOfDigits;
                currentDigit = 0;
                numOfGuesses = 0;
                swapPhrase = false;
                enter();
                return;
        }
    }
}

void GuessTheNumber::exit() {
    lcd.noBlink();
    numOfDigits = 1;
    currentDigit = 0;
    numOfGuesses = 0;
    swapPhrase = false;
}

void GuessTheNumber::generateNumber() {
    generatedNumber[0] = random(1, 10);
    for (uint8_t i = 1; i < numOfDigits; ++i)
        generatedNumber[i] = random(10);
}

void GuessTheNumber::printGuess() {
    char guessString[16] = {0};
    for (uint8_t i = 0; i < sizeOfCurrentGuess; ++i)
        guessString[i] = currentGuess[i] != -1 ? decDigits[currentGuess[i]] : ' ';
    writeLineToRow(guessString, 1);
}

void GuessTheNumber::resetGuess() {
    for (uint8_t i = 0; i < sizeOfCurrentGuess; ++i)
        currentGuess[i] = i < numOfDigits ? 0 : -1;
}

int8_t GuessTheNumber::checkCurrentGuess() {
    for (uint8_t i = 0; i < numOfDigits; ++i) {
        if (currentGuess[i] == generatedNumber[i])
            continue;
        return currentGuess[i] > generatedNumber[i] ? 1 : -1;
    }
    return 0;
}

/* JumpTheRock */
void JumpTheRock::enter() {
    writeLineToRow(nullptr, 0);
    writeLineToRow(nullptr, 1);
    lcdCreateChars();
    cycle();
    timer = millis();
}

void JumpTheRock::loop() {
    static unsigned long selectClickTime = 0;
    static uint16_t pauseDifference = 0;
    if (btnSelect.justClicked) 
        selectClickTime = millis();
    if (btnSelect.btnCurrentState && millis() - selectClickTime > 1500) {
        changeMenu(previousMenu);
        return;
    }
    if (btnSelect.justReleased){
        paused = !paused;
        if (paused)
            pauseDifference = millis() - timer;
        else
            timer = millis() - pauseDifference;
    }
    if (paused)
        return;
    if (playerState == PlayerState::Running) {
        if (btnLeft.justClicked)
            if (!(playerPos >> 15 & 0b1)) {
                playerPos = playerPos << 1;
                checkCollision();
                renderScreen();
            }
        if (btnRight.justClicked)
            if (!(playerPos & 0b1)) {
                playerPos = playerPos >> 1;
                checkCollision();
                renderScreen();
            }
        if (btnUp.justClicked) {
            playerState = PlayerState::Jumping;
            renderScreen();
        }
    }
    if (millis() - timer > updateInterval) {
        cycle();
        timer = millis();
    }
}

void JumpTheRock::exit() {
    resetGame();
}

void JumpTheRock::resetGame() {
    score = -1;
    path = 0;
    playerPos = 0b1 << 15;
    playerState = PlayerState::Running;
    currentChance = 20;
    paused = false;
}

void JumpTheRock::lcdCreateChars() {
    for (int i = 0; i < sizeOfCustomChars; ++i)
        lcd.createChar(i, customDisplayCharacters[i]);
}

void JumpTheRock::renderScreen() {
    static bool runningSprite = 0;
    writeLineToRow("Score:");
    char buffer[11] = {0};
    ltoa(score, buffer, 10);
    writeLineToRowRightNoClear(buffer);
    auto pathString = int16StringRep(path);
    for (uint8_t i = 0; i < pathString.size; ++i)
        if (playerPos >> i & 0b1) {
            pathString[pathString.size - 1 - i] = playerRep;
            break;
        }
    lcd.setCursor(0, 1);
    for (uint8_t i = 0; i < pathString.size; ++i) {
        if (pathString[i] == stoneRep)
            lcd.write(stoneSprite);
        else if (pathString[i] == playerRep)
            switch (playerState) {
                case PlayerState::Running:
                    lcd.write(runningSprite);
                    runningSprite = !runningSprite;
                    break;
                case PlayerState::Jumping: 
                    lcd.write(playerPos & path ? jumpingStoneSprite : jumpingSprite);
                    break;
                case PlayerState::Dead:
                    lcd.write(deathSprite);
                    break;
            }
        else
            lcd.write(' ');
    }
}

void JumpTheRock::checkCollision() {
    if (playerPos & path && playerState == PlayerState::Running) {
        playerState = PlayerState::Dead;
        playerPos >>= 1;
        renderScreen();
        writeLineToRow("You died :(");
        delay(3000);
        renderScreen();
        char buffer[14] = "Resetting.. ";
        buffer[13] = 0;
        for (char i = '5'; i != '0'; --i) {
            buffer[12] = i;
            writeLineToRow(buffer, 1);
            delay(1000);
        }
        resetGame();
    }
    score += (playerPos & path && playerState == PlayerState::Jumping) ? 5 : 1;
}

void JumpTheRock::cycle() {
    static bool wasJumping = false;
    if (wasJumping) {
        playerState = PlayerState::Running;
        wasJumping = false;
    }
    if (playerState == PlayerState::Jumping)
        wasJumping = true;
    path <<= 1;
    if (!(path & 0b111) && random(1, 101) < currentChance){
        path |= 0b01;
        if (++currentChance > 101)
            currentChance = 101;
    }
    checkCollision();
    renderScreen();
}

#endif //FUNCTIONS_HEADER
