#ifndef VARIABLES_HEADER
#define VARIABLES_HEADER

#include "includes.h"
#include "defines.h"
#include "structs.h"

//pints 4 to 10 and A0 used by lcd shield
const uint8_t pinButtons = A0, pinRFIDReader = 3;
const uint8_t lcdCols = 16, lcdRows = 2;
const char hexDigits[] = "0123456789ABCDEF";
const char decDigits[] = "0123456789";
const char binaryDigits[] = "01";

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

Button btnSelect(BUTTON_SELECT_VALUES), btnLeft(BUTTON_LEFT_VALUES), btnDown(BUTTON_DOWN_VALUES), btnUp(BUTTON_UP_VALUES), btnRight(BUTTON_RIGHT_VALUES);
Button *buttons[] = {&btnSelect, &btnLeft, &btnDown, &btnUp, &btnRight};
const uint8_t buttonsSize = sizeof(buttons) / sizeof(Button *);

MainMenu mainMenu("main menu");
GuessTheNumber guessTheNumber("Guess the number", &mainMenu);
JumpTheRock jumpTheRock("Jump the rock", &mainMenu);

const MenuTemplate *MainMenu::subMenus[] = {&guessTheNumber, &jumpTheRock};
const uint8_t MainMenu::subMenusSize = sizeof(MainMenu::subMenus) / sizeof(MenuTemplate*);

const uint8_t JumpTheRock::customDisplayCharacters[][8] = {
    { //running
        0b00000,
        0b00110,
        0b11110,
        0b10111,
        0b00110,
        0b00101,
        0b01001,
        0b10010
    },
    { //running
        0b00000,
        0b01100,
        0b11101,
        0b11111,
        0b01100,
        0b00100,
        0b11010,
        0b00010
    },
    { //jumping
        0b01101,
        0b11110,
        0b01110,
        0b11001,
        0b00000,
        0b00000,
        0b00000,
        0b00000
    },
    { //death
        0b00000,
        0b01001,
        0b01001,
        0b00110,
        0b10110,
        0b10111,
        0b01111,
        0b00110
    },
    { //rock
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b01100,
        0b11110,
        0b11111,
        0b11111
    },
    { //jumping rock
        0b01101,
        0b11110,
        0b01110,
        0b11001,
        0b01100,
        0b11110,
        0b11111,
        0b11111
    }
};
const uint8_t JumpTheRock::sizeOfCustomChars = sizeof(customDisplayCharacters) / sizeof(uint8_t[8]);
const uint8_t JumpTheRock::jumpingSprite = 2;
const uint8_t JumpTheRock::deathSprite = 3;
const uint8_t JumpTheRock::stoneSprite = 4;
const uint8_t JumpTheRock::jumpingStoneSprite = 5;
const uint16_t JumpTheRock::updateInterval = 500;
const char JumpTheRock::emptySpaceRep = '0';
const char JumpTheRock::stoneRep = '1';
const char JumpTheRock::playerRep = '2';

uint8_t MainMenu::currentOption = 0;

MenuTemplate *currentMenu = &mainMenu;

#endif //VARIABLES_HEADER
