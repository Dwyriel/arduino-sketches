#include "includes.h"
#include "defines.h"
#include "structs.h"
#include "variables.h"
#include "functions.h"

void setup() {
    lcd.begin(lcdCols, lcdRows);
    writeLineToRow("Initializing...");
    randomSeed((((unsigned long) analogRead(A5) * micros()) / (analogRead(A4) + millis())) + analogRead(A0));
    pinMode(pinButtons, INPUT);
    currentMenu->enter();
}

void loop() {
    checkButtonPress();
    currentMenu->loop();
}
