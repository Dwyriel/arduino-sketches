#ifndef STRUCTS_HEADER
#define STRUCTS_HEADER

#include "defines.h"

struct Button {
private:
    static const uint8_t clickDebounceDelay = 10;
    unsigned long btnClickTime = 0;
    uint16_t pressedValueLow, pressedValueHigh;
    bool btnPreviousValue = false;

public:
    bool justClicked = false;
    bool justReleased = false;
    bool btnCurrentState = false;

    Button(uint16_t pressedValueLow, uint16_t pressedValueHigh) {
        if (pressedValueLow > pressedValueHigh) {
            uint16_t temp = pressedValueHigh;
            pressedValueHigh = pressedValueLow;
            pressedValueLow = temp;
        }
        this->pressedValueHigh = pressedValueHigh;
        this->pressedValueLow = pressedValueLow;
    }

    void checkIfButtonPressed(uint16_t value) {
        justClicked = false;
        justReleased = false;
        bool btnCurrentValue = value >= pressedValueLow && value <= pressedValueHigh;
        if (btnCurrentValue != btnPreviousValue)
            btnClickTime = millis();
        if ((millis() - btnClickTime) > clickDebounceDelay && btnCurrentValue != btnCurrentState) {
            btnCurrentState = btnCurrentValue;
            justClicked = btnCurrentState;
            justReleased = !btnCurrentState;
        }
        btnPreviousValue = btnCurrentValue;
    }
};

template<typename T, size_t arrSize = INT16_NUM_BITS>
struct array {
    size_t size = arrSize;
    T data[arrSize] = {0};

    inline T& operator[](size_t index) {
        return data[index];
    }

    inline const T& operator[](size_t index) const {
        return data[index];
    }
};

using arrayChar16 = array<char, INT16_NUM_BITS>;

struct MenuTemplate {
protected:
    MenuTemplate *previousMenu;

public:
    const char *title;

    MenuTemplate(const char *title, MenuTemplate *previousMenu = nullptr);
    
    virtual void enter();

    virtual void loop();

    virtual void exit();
};

struct MainMenu : public MenuTemplate {
private:
    static const MenuTemplate *subMenus[];
    static const uint8_t subMenusSize;
    static uint8_t currentOption;

    bool justStarted = true;

    void showOption();

public:
    MainMenu(const char *title, MenuTemplate *previousMenu = nullptr);

    void enter() override;

    void loop() override;

    void exit() override;
};

struct GuessTheNumber : public MenuTemplate {
private:
    int8_t generatedNumber[16] = {0};
    int8_t currentGuess[16] = {0};
    uint8_t sizeOfCurrentGuess = sizeof(currentGuess) / sizeof(int8_t);
    uint8_t numOfDigits = 1;
    uint8_t currentDigit = 0;
    uint16_t numOfGuesses = 0;
    bool swapPhrase = false;

    void generateNumber();

    void printGuess();

    void resetGuess();

    int8_t checkCurrentGuess();

public:
    GuessTheNumber(const char *title, MenuTemplate *previousMenu = nullptr);

    void enter() override;

    void loop() override;

    void exit() override;
};

struct JumpTheRock : public MenuTemplate {
private:
    static const uint8_t customDisplayCharacters[][8];
    static const uint8_t sizeOfCustomChars;
    static const uint8_t jumpingSprite;
    static const uint8_t deathSprite;
    static const uint8_t stoneSprite;
    static const uint8_t jumpingStoneSprite;
    static const uint16_t updateInterval;
    static const char emptySpaceRep, stoneRep, playerRep;

    enum class PlayerState : uint8_t {
        Running,
        Jumping,
        Dead
    };

    unsigned long timer = 0;
    unsigned long score = -1;
    uint16_t path = 0;
    uint16_t playerPos = 0b1 << 15;
    PlayerState playerState = PlayerState::Running;
    uint8_t currentChance = 20;
    bool paused = false;

    void resetGame();

    void lcdCreateChars();

    void renderScreen();

    void checkCollision();

    void cycle();

public:
    JumpTheRock(const char *title, MenuTemplate *previousMenu = nullptr);

    void enter() override;

    void loop() override;

    void exit() override;
};

#endif //STRUCTS_HEADER
