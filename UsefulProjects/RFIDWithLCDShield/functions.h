#ifndef FUNCTIONS_HEADER
#define FUNCTIONS_HEADER

#include "includes.h"
#include "structs.h"
#include "variables.h"

void checkButtonPress() {
    uint16_t readValue = analogRead(pinButtons);
    for (uint8_t i = 0; i < buttonsSize; ++i) //looping through all of them all the time to make sure we know which button is pressed and easily set if a button was "just pressed", slower than manually doing it with "if else"s but better to work with.
        buttons[i]->checkIfButtonPressed(readValue);
}

void uidArrayToUidString(uint8_t uidToConvert[UID_SIZE] = uid) {
    for (uint8_t i = 0, j = 2; i < UID_SIZE; ++i) {
        uidString[j++] = hexDigits[(uidToConvert[i] >> 4)];
        uidString[j++] = hexDigits[(uidToConvert[i] & 0b00001111)];
    }
}

uint8_t numFromHexDigit(char hex) {
    for (uint8_t i = 0; i < 16; i++)
        if (hexDigits[i] == hex)
            return i;
    return 0xFF;
}

void writeLineToRow(const char *line, uint8_t row = 0) {
    uint8_t leftover = 0;
    lcd.setCursor(0, row);
    for (uint8_t i = 0; i < (line != nullptr ? lcdCols : 0); ++i) {
        if (line[i] == '\0') {
            leftover = lcdCols - i;
            break;
        }
        lcd.write(line[i]);
    }
    for (uint8_t i = 0; i < leftover; ++i)
        lcd.write(' ');
}

void writeLineToRowNoClear(const char *line, uint8_t row = 0) {
    if (line == nullptr)
        return;
    lcd.setCursor(0, row);
    for (uint8_t i = 0; i < lcdCols; ++i) {
        if (line[i] == '\0')
            return;
        lcd.write(line[i]);
    }
}

void writeLineToRowRight(const char *line, uint8_t row = 0) {
    if (line == nullptr)
        return writeLineToRow(line, row);
    lcd.rightToLeft();
    uint8_t lineSize = lcdCols;
    uint8_t leftover = 0;
    lcd.setCursor(lcdCols - 1, row);
    for (uint8_t i = 0; i < lcdCols; ++i)
        if (line[i] == '\0') {
            lineSize = i;
            leftover = lcdCols - i;
            break;
        }
    for (uint8_t i = lineSize - 1; i != 0xFF; --i)
        lcd.write(line[i]);
    for (uint8_t i = 0; i < leftover; ++i)
        lcd.write(' ');
    lcd.leftToRight();
}

void writeLineToRowRightNoClear(const char *line, uint8_t row = 0) {
    if (line == nullptr)
        return;
    lcd.rightToLeft();
    uint8_t lineSize = lcdCols;
    lcd.setCursor(lcdCols - 1, row);
    for (uint8_t i = 0; i < lcdCols; ++i)
        if (line[i] == '\0') {
            lineSize = i;
            break;
        }
    for (uint8_t i = lineSize - 1; i != 0xFF; --i)
        lcd.write(line[i]);
    lcd.leftToRight();
}

void changeState(State *nextState) {
    currentState->exit();
    currentState = nextState;
    currentState->enter();
}

void readUID(uint8_t *_uid) {
    for (uint8_t i = 0; i < UID_SIZE; ++i)
        _uid[i] = mfrc522.uid.uidByte[i];
    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();
}

void saveUID(uint8_t _uid[UID_SIZE] = uid) {
    for (uint8_t i = 0; i < UID_SIZE; ++i)
        EEPROM.update(i, _uid[i]);
}

void printWritingUID() {
    writeLineToRow("Writing UID:");
    writeLineToRow(uidString, 1);
}

void writeUIDFailed() {
    writeLineToRow("Could not be");
    writeLineToRowRight("written.", 1);
    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();
}

void writeUIDToCard(uint8_t _uid[UID_SIZE] = uid) {
    bool success = mfrc522Hack.MIFARE_SetUid(_uid, (byte) UID_SIZE, key, true);
    if (success) {
        mfrc522.PICC_HaltA();
        mfrc522.PCD_StopCrypto1();
        writeLineToRow("UID Written.");
        writeLineToRow("Returning...", 1);
        delay(WAIT_TIME_AFTER_ACTION);
        changeState(&stateMenu);
        return;
    } else { //! sometimes the PCD hangs when trying to write to unwritable tags, sometimes it doesn't. Not even soft resetting solved it.
        writeUIDFailed();
        delay(WAIT_TIME_AFTER_ACTION);
        printWritingUID();
    }
}

void Menu::enter() {
    uidArrayToUidString();
    writeLineToRow(entries[currentOption].menuEntry);
    writeLineToRowRight(uidString, 1);
    writeLineToRowNoClear(" Mem:", 1);
    mfrc522.PCD_SoftPowerDown();
}

void Menu::loop() {
    if (btnDown.justClicked) {
        if (++currentOption == entriesSize)
            currentOption = 0;
        writeLineToRow(entries[currentOption].menuEntry);
    }
    if (btnUp.justClicked) {
        if (--currentOption == 0xFF)
            currentOption = entriesSize - 1;
        writeLineToRow(entries[currentOption].menuEntry);
    }
    if (btnRight.justClicked || btnSelect.justClicked) {
        if (currentOption >= entriesSize) {
            writeLineToRow("Not implemented");
            return;
        }
        changeState(entries[currentOption].state);
    }
}

void Menu::exit() {
    mfrc522.PCD_SoftPowerUp();
}

void ReadUID::enter() {
    writeLineToRow("Waiting for RFID", 0);
    writeLineToRow("Card or Tag.", 1);
}

void ReadUID::loop() {
    if (btnLeft.justClicked) {
        changeState(&stateMenu);
        return;
    }
    if (uidRead && (btnRight.justClicked || btnSelect.justClicked)) {
        for (uint8_t i = 0; i < UID_SIZE; ++i)
            uid[i] = tempUID[i];
        saveUID(uid);
        writeLineToRow("Saved!");
        writeLineToRow("Returning...", 1);
        delay(WAIT_TIME_AFTER_ACTION);
        changeState(&stateMenu);
    }
    if (!mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial())
        return;
    readUID(tempUID);
    uidRead = true;
    uidArrayToUidString(tempUID);
    char line1[16] = "UID: ";
    for (uint8_t i = 5; i < 16; ++i)
        line1[i] = uidString[i - 5];
    writeLineToRow(line1, 0);
    writeLineToRow("Select to save.", 1);
}

void ReadUID::exit() {
    uidRead = false;
}

void EditUID::enter() {
    for (uint8_t i = 0; i < 10; ++i)
        tempUIDString[i] = uidString[i];
    tempUIDString[10] = '\0';
    writeLineToRow("Editing UID:");
    writeLineToRow(tempUIDString, 1);
    lcd.setCursor(currentIndex, 1);
    lcd.blink();
}

void EditUID::loop() {
    if (btnSelect.justClicked)
        saveAndExit();
    if (btnLeft.justClicked) {
        if (currentIndex == 2) {
            changeState(&stateMenu);
            return;
        }
        lcd.setCursor(--currentIndex, 1);
    }
    if (btnRight.justClicked) {
        if (currentIndex == 9) {
            saveAndExit();
            return;
        }
        lcd.setCursor(++currentIndex, 1);
    }
    if (btnUp.justClicked) {
        uint8_t val = numFromHexDigit(tempUIDString[currentIndex]);
        if (++val == 16)
            val = 0;
        tempUIDString[currentIndex] = hexDigits[val];
        writeLineToRow(tempUIDString, 1);
        lcd.setCursor(currentIndex, 1);
    }
    if (btnDown.justClicked) {
        uint8_t val = numFromHexDigit(tempUIDString[currentIndex]);
        if (--val == 0xFF)
            val = 15;
        tempUIDString[currentIndex] = hexDigits[val];
        writeLineToRow(tempUIDString, 1);
        lcd.setCursor(currentIndex, 1);
    }
}

void EditUID::exit() {
    currentIndex = 2;
    lcd.noBlink();
}

void EditUID::saveAndExit() {
    for (uint8_t i = 0, j = 2; i < UID_SIZE; ++i, j += 2)
        uid[i] = (numFromHexDigit(tempUIDString[j]) << 4) | numFromHexDigit(tempUIDString[j + 1]);
    saveUID(uid);
    changeState(&stateMenu);
}

void WriteUID::enter() {
    printWritingUID();
}

void WriteUID::loop() {
    if (btnLeft.justClicked) {
        changeState(&stateMenu);
        return;
    }
    if (!mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial())
        return;
    writeUIDToCard();
}

void WriteUID::exit() {
}

void RandomUID::enter() {
    randomize();
    uidArrayToUidString(randUID);
    showRandomUID();
}

void RandomUID::loop() {
    if (btnLeft.justClicked) {
        if (writeToCard) {
            writeToCard = false;
            showRandomUID();
            return;
        }
        changeState(&stateMenu);
        return;
    }
    if (writeToCard) {
        if (!mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial())
            return;
        writeUIDToCard(randUID);
    }
    if (btnSelect.justClicked) {
        for (uint8_t i = 0; i < UID_SIZE; ++i)
            uid[i] = randUID[i];
        saveUID(randUID);
        writeLineToRow("UID Saved.");
        writeLineToRow("Returning...", 1);
        delay(WAIT_TIME_AFTER_ACTION);
        changeState(&stateMenu);
        return;
    }
    if (btnRight.justClicked) {
        printWritingUID();
        writeToCard = true;
    }
    if (btnUp.justClicked || btnDown.justClicked) {
        randomize();
        uidArrayToUidString(randUID);
        showRandomUID();
    }
}

void RandomUID::exit() {
    writeToCard = false;
}

void RandomUID::randomize() {
    for (uint8_t i = 0; i < UID_SIZE; ++i)
        randUID[i] = random(256);
}

void RandomUID::showRandomUID() {
    writeLineToRow("Gen. UID:");
    writeLineToRowRight(uidString, 1);
}

void CloneUID::enter() {
    printEnteringTitleToLCD();
}

void CloneUID::loop() {
    if (btnLeft.justClicked) {
        if (tagRead) {
            tagRead = false;
            printEnteringTitleToLCD();
            return;
        }
        changeState(&stateMenu);
        return;
    }
    if (!mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial())
        return;
    if (!tagRead) {
        readUID(tempUID);
        uidArrayToUidString(tempUID);
        printWritingUID();
        tagRead = true;
    } else
        writeUIDToCard(tempUID);
}

void CloneUID::exit() {
    tagRead = false;
}

void CloneUID::printEnteringTitleToLCD() {
    writeLineToRow("Cloning UID");
    writeLineToRow("Waiting for tag.", 1);
}

#endif //FUNCTIONS_HEADER
