#ifndef STRUCTS_HEADER
#define STRUCTS_HEADER

struct Button {
private:
    static const uint8_t clickDebounceDelay = 10;
    unsigned long btnClickTime = 0;
    uint16_t pressedValueLow, pressedValueHigh;
    bool btnPreviousValue = false;

public:
    bool justClicked = false;
    bool btnCurrentState = false;

    Button(uint16_t pressedValueLow, uint16_t pressedValueHigh) {
        if (pressedValueLow > pressedValueHigh) {
            uint16_t temp = pressedValueHigh;
            pressedValueHigh = pressedValueLow;
            pressedValueLow = temp;
        }
        this->pressedValueHigh = pressedValueHigh;
        this->pressedValueLow = pressedValueLow;
    }

    void checkIfButtonPressed(uint16_t value) {
        justClicked = false;
        bool btnCurrentValue = value >= pressedValueLow && value <= pressedValueHigh;
        if (btnCurrentValue != btnPreviousValue)
            btnClickTime = millis();
        if ((millis() - btnClickTime) > clickDebounceDelay && btnCurrentValue != btnCurrentState) {
            btnCurrentState = btnCurrentValue;
            justClicked = btnCurrentState;
        }
        btnPreviousValue = btnCurrentValue;
    }
};

struct State {
    virtual void enter();

    virtual void loop();

    virtual void exit();
};

struct MenuEntry {
    const char *menuEntry;
    State *state;
};

struct Menu : public State {
private:
    static const MenuEntry entries[];
    static const uint8_t entriesSize;
    uint8_t currentOption = 0;

public:
    void enter() override;

    void loop() override;

    void exit() override;
};

struct ReadUID : public State {
private:
    uint8_t tempUID[4] = {0x00, 0x00, 0x00, 0x00};
    bool uidRead = false;

public:
    void enter() override;

    void loop() override;

    void exit() override;
};

struct EditUID : public State {
private:
    uint8_t currentIndex = 2;
    char tempUIDString[11];

    void saveAndExit();

public:
    void enter() override;

    void loop() override;

    void exit() override;
};

struct WriteUID : public State {
public:
    void enter() override;

    void loop() override;

    void exit() override;
};

struct RandomUID : public State {
private:
    uint8_t randUID[4] = {0x00, 0x00, 0x00, 0x00};
    bool writeToCard = false;

    void randomize();

    void showRandomUID();

public:
    void enter() override;

    void loop() override;

    void exit() override;
};

struct CloneUID : public State {
private:
    uint8_t tempUID[4] = {0x00, 0x00, 0x00, 0x00};
    bool tagRead = false;

    void printEnteringTitleToLCD();

public:
    void enter() override;

    void loop() override;

    void exit() override;
};

#endif //STRUCTS_HEADER
