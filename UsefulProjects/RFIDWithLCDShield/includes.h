#ifndef INCLUDES_HEADER
#define INCLUDES_HEADER

#include <Arduino.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <MFRC522v2.h>
#include <MFRC522DriverSPI.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Hack.h>

#endif //INCLUDES_HEADER
