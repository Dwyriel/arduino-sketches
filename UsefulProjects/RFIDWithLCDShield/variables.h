#ifndef VARIABLES_HEADER
#define VARIABLES_HEADER

#include "includes.h"
#include "structs.h"

#define UID_SIZE 4
#define HEX_PREFIX "0x"
#define BUTTON_SELECT_VALUES 600, 700
#define BUTTON_LEFT_VALUES 400, 500
#define BUTTON_DOWN_VALUES 200, 300
#define BUTTON_UP_VALUES 90, 150
#define BUTTON_RIGHT_VALUES 0, 50
#define WAIT_TIME_AFTER_ACTION 3000

//pins 4 to 10 and A0 used by lcd shield, pins 11, 12 and 13 used for SPI communication
const uint8_t pinButtons = A0, pinRFIDReader = 3;
const uint8_t lcdCols = 16, lcdRows = 2;
const char hexDigits[] = "0123456789ABCDEF";

//SCK > 13 | MOSI > 11 | MISO > 12 | SDI > user decide
MFRC522DriverPinSimple ss_pin(pinRFIDReader);
MFRC522DriverSPI driver(ss_pin);
MFRC522 mfrc522(driver);
MFRC522Hack mfrc522Hack(mfrc522, false);
MFRC522::MIFARE_Key key;
uint8_t uid[4] = {0xFF, 0xFF, 0xFF, 0xFF};
uint8_t uidString[11] = "0xFFFFFFFF\0";

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

Button btnSelect(BUTTON_SELECT_VALUES), btnLeft(BUTTON_LEFT_VALUES), btnDown(BUTTON_DOWN_VALUES), btnUp(BUTTON_UP_VALUES), btnRight(BUTTON_RIGHT_VALUES);
Button *buttons[] = {&btnSelect, &btnLeft, &btnDown, &btnUp, &btnRight};
const uint8_t buttonsSize = sizeof(buttons) / sizeof(Button *);

Menu stateMenu;
ReadUID stateReadUID;
EditUID stateEditUID;
WriteUID stateWriteUID;
RandomUID stateRandomUID;
CloneUID stateCloneUID;
State *currentState = &stateMenu;
const MenuEntry Menu::entries[] = {{"1.Read UID", &stateReadUID}, {"2.Edit UID", &stateEditUID}, {"3.Write UID", &stateWriteUID}, {"4.Random UID", &stateRandomUID}, {"5.Clone UID", &stateCloneUID}};
const uint8_t Menu::entriesSize = sizeof(Menu::entries) / sizeof(MenuEntry);

#endif //VARIABLES_HEADER
