#include "includes.h"
#include "structs.h"
#include "variables.h"
#include "functions.h"

void setup() {
    lcd.begin(lcdCols, lcdRows);
    writeLineToRow("Initializing...");
    randomSeed((analogRead(A5) * micros()) / (analogRead(A4) + millis()));
    pinMode(pinButtons, INPUT);
    bool mfrcInitSuccess = mfrc522.PCD_Init();
    if (!mfrcInitSuccess) {
        writeLineToRow("Init failed");
        writeLineToRow("Check reader dev", 1);
        while (true) {(void)0;}
    }
    for (byte i = 0; i < 6; ++i)
        key.keyByte[i] = 0xFF;
    for (int i = 0; i < 4; ++i)
        uid[i] = EEPROM.read(i);
    currentState->enter();
}

void loop() {
    checkButtonPress();
    currentState->loop();
}
