//requirements: 3 LEDs

void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

struct led {
    int pin;
    int delay;
};

const struct led leds[] = {{8,  5000}, {9,  2000}, {10, 5000}};
const int led_size = sizeof(leds) / sizeof(led);

void setup() {
    disableBuiltinLED();
    for (int i = 0; i < led_size; i++)
        pinMode(leds[i].pin, OUTPUT);
}

void loop() {
    for (int i = 0; i < led_size; i++) {
        digitalWrite(leds[i].pin, HIGH);
        delay(leds[i].delay);
        digitalWrite(leds[i].pin, LOW);
    }
}
