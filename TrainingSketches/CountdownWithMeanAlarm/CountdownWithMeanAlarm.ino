//requirements: 1 button, 1 seven segment display, 1 buzzer and optionally 1 LED

void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

//Change to 0 if using a CC (Common Cathode) display
#define anodeDisplay 1
#if anodeDisplay
#define ON LOW
#define OFF HIGH
#else
#define ON HIGH
#define OFF LOW
#endif
#undef anodeDisplay

const uint8_t btnPin = 12;
const uint8_t ledPin = 11;
const uint8_t buzzerPin = 10;
const unsigned long clickDebounceDelay = 20;
const uint8_t segA = 0;
const uint8_t segB = 1;
const uint8_t segC = 2;
const uint8_t segD = 3;
const uint8_t segE = 4;
const uint8_t segF = 5;
const uint8_t segG = 6;
const uint8_t segDP = 7;
const uint8_t segmentPins[] = {2, 3, 4, 5, 6, 7, 8, 9}; //A, B, C... G, DP
const uint8_t segments_size = sizeof(segmentPins) / sizeof(uint8_t);
uint8_t segments[segments_size] = {OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF};

void resetSegments() {
    for (uint8_t i = 0; i < segments_size; ++i)
        segments[i] = OFF;
}

void displayWrite() {
    for (uint8_t i = 0; i < segments_size; ++i)
        digitalWrite(segmentPins[i], segments[i]);
}

void displayZero() {
    resetSegments();
    segments[segA] = ON;
    segments[segB] = ON;
    segments[segC] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    displayWrite();
}

void displayOne() {
    resetSegments();
    segments[segB] = ON;
    segments[segC] = ON;
    displayWrite();
}

void displayTwo() {
    resetSegments();
    segments[segA] = ON;
    segments[segB] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayThree() {
    resetSegments();
    segments[segA] = ON;
    segments[segB] = ON;
    segments[segC] = ON;
    segments[segD] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayFour() {
    resetSegments();
    segments[segB] = ON;
    segments[segC] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayFive() {
    resetSegments();
    segments[segA] = ON;
    segments[segC] = ON;
    segments[segD] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displaySix() {
    resetSegments();
    segments[segA] = ON;
    segments[segC] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displaySeven() {
    resetSegments();
    segments[segA] = ON;
    segments[segB] = ON;
    segments[segC] = ON;
    displayWrite();
}

void displayEight() {
    resetSegments();
    segments[segA] = ON;
    segments[segB] = ON;
    segments[segC] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayNine() {
    resetSegments();
    segments[segA] = ON;
    segments[segB] = ON;
    segments[segC] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayA() {
    resetSegments();
    segments[segA] = ON;
    segments[segB] = ON;
    segments[segC] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayB() {
    resetSegments();
    segments[segC] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayC() {
    resetSegments();
    segments[segA] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    displayWrite();
}

void displayD() {
    resetSegments();
    segments[segB] = ON;
    segments[segC] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayE() {
    resetSegments();
    segments[segA] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayF() {
    resetSegments();
    segments[segA] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    segments[segG] = ON;
    displayWrite();
}

void displayU() {
    resetSegments();
    segments[segB] = ON;
    segments[segC] = ON;
    segments[segD] = ON;
    segments[segE] = ON;
    segments[segF] = ON;
    displayWrite();
}

void (*const displayNumberFunctions[])(void) = {displayZero, displayOne, displayTwo, displayThree, displayFour, displayFive, displaySix, displaySeven, displayEight, displayNine};

unsigned long btnClickTime = 0;
bool btnPreviousValue = 0, btnCurrentState = 0, justClicked = false;

void checkIfButtonPressed() {
    justClicked = false;
    bool btnCurrentValue = digitalRead(btnPin);
    if (btnCurrentValue != btnPreviousValue)
        btnClickTime = millis();
    if ((millis() - btnClickTime) > clickDebounceDelay && btnCurrentValue != btnCurrentState) {
        btnCurrentState = btnCurrentValue;
        justClicked = btnCurrentState;
    }
    btnPreviousValue = btnCurrentValue;
}

bool F_or_U = true, pauseCD = false, stopAlarm = false, alarmRinging = false, ringAlarm = false;
int counter = 9;
unsigned long milis = 0, milisAlarm = 0, difference = 0;

void setup() {
    disableBuiltinLED();
    pinMode(btnPin, INPUT);
    pinMode(ledPin, OUTPUT);
    pinMode(buzzerPin, OUTPUT);
    for (uint8_t i = 0; i < segments_size; ++i)
        pinMode(segmentPins[i], OUTPUT);
    analogWrite(buzzerPin, 0);
    displayNine();
    displayWrite();
    milis = millis();
}

void loop() {
    checkIfButtonPressed();
    if (counter > 1 && counter < 10) {
        if (justClicked) {
            pauseCD = !pauseCD;
            if (pauseCD)
                difference = millis() - milis;
            else
                milis = millis() - difference;
        }
        if (pauseCD)
            return;
        if (millis() - milis > 1000) {
            milis = millis();
            (*displayNumberFunctions[--counter])();
        }
        return;
    }
    if (justClicked) {
        stopAlarm = !stopAlarm;
        milis = 0;
        alarmRinging = false;
        ringAlarm = false;
        digitalWrite(ledPin, LOW);
        analogWrite(buzzerPin, 0);
    }
    if (stopAlarm)
        return;
    if (millis() - milis > 1000) {
        milis = millis();
        if (F_or_U)
            displayF();
        else
            displayU();
        F_or_U = !F_or_U;
        milisAlarm = milis;
        alarmRinging = true;
        digitalWrite(ledPin, HIGH);
        analogWrite(buzzerPin, 255);
    }
    if (alarmRinging) {
        if (millis() - milis > 500) {
            alarmRinging = false;
            ringAlarm = false;
            digitalWrite(ledPin, LOW);
            analogWrite(buzzerPin, 0);
        }
        if (millis() - milisAlarm > 100) {
            milisAlarm = millis();
            if (ringAlarm) {
                digitalWrite(ledPin, HIGH);
                analogWrite(buzzerPin, 255);
            } else {
                digitalWrite(ledPin, LOW);
                analogWrite(buzzerPin, 0);
            }
            ringAlarm = !ringAlarm;
        }
    }
}
