void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

const uint8_t pinButton = 7, pinLED = 8, pinBuzzer = 2, pinSensor = A0, pinIRreceiver = 12;
const unsigned long clickDebounceDelay = 20;
unsigned long btnClickTime = 0, milis = 0;
bool btnPreviousValue = 0, btnCurrentState = 0, justClicked = false, paused = false, alarmON = false, prevAlarmValue = false, buzzing = false;

void checkIfButtonPressed() {
    justClicked = false;
    bool btnCurrentValue = digitalRead(pinButton);
    if (btnCurrentValue != btnPreviousValue)
        btnClickTime = millis();
    if ((millis() - btnClickTime) > clickDebounceDelay && btnCurrentValue != btnCurrentState) {
        btnCurrentState = btnCurrentValue;
        justClicked = btnCurrentState;
    }
    btnPreviousValue = btnCurrentValue;
}

void setup() {
    disableBuiltinLED();
    pinMode(pinButton, INPUT);
    pinMode(pinSensor, INPUT);
    pinMode(pinBuzzer, OUTPUT);
    pinMode(pinLED, OUTPUT);
    digitalWrite(pinBuzzer, HIGH);
    digitalWrite(pinLED, !paused);
}

void loop() {
    alarmON = digitalRead(pinSensor);
    if (alarmON != prevAlarmValue){
        prevAlarmValue = alarmON;
        if (!alarmON){
            digitalWrite(pinBuzzer, HIGH);
            digitalWrite(pinLED, !paused);
            buzzing = false;
        }
    }
    checkIfButtonPressed();
    if (justClicked){
        paused = !paused;
        milis = 0;
        digitalWrite(pinBuzzer, HIGH);
        digitalWrite(pinLED, !paused);
    }
    if(paused)
        return;
    if(alarmON){
        if(millis() - milis > 200){
            milis = millis();
            digitalWrite(pinBuzzer, buzzing);
            digitalWrite(pinLED, buzzing);
            buzzing = !buzzing;
        }
    }
}
