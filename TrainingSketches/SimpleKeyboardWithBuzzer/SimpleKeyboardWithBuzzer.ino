//requirements: a few buttons and a buzzer.

void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

struct Button {
    uint8_t pin;
    unsigned int frequency;
    bool currentState = 0;

    explicit Button(uint8_t pin) : pin(pin), frequency(65535) {};

    explicit Button(uint8_t pin, unsigned int freq) : pin(pin), frequency(freq) {};

private:
    static const unsigned long clickDebounceDelay = 10;
    bool previousValue = 0xFF;
    unsigned long clickTime = 0;

public:
    void checkIfWasPressed() {
        bool currentValue = digitalRead(this->pin);
        if (currentValue != this->previousValue)
            this->clickTime = millis();
        if ((millis() - this->clickTime) > clickDebounceDelay && currentValue != this->currentState)
            this->currentState = currentValue;
        this->previousValue = currentValue;
    }
};

const uint8_t buzzerPin = 3;
Button btns[] = {Button(12, 0x17FF), Button(11, 0x0FFF)};
const uint8_t btns_size = sizeof(btns) / sizeof(Button);

void setup() {
    disableBuiltinLED();
    for (int i = 0; i < btns_size; ++i)
        pinMode(btns[i].pin, INPUT);
    pinMode(buzzerPin, OUTPUT);
}

void loop() {
    unsigned int frequency = 0;
    for (int i = 0; i < btns_size; ++i) {
        btns[i].checkIfWasPressed();
        if (btns[i].currentState)
            frequency |= btns[i].frequency;
    }
    if (frequency)
        tone(buzzerPin, frequency);
    else
        noTone(buzzerPin);
}
