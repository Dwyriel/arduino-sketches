//requirements: 1 button, array of leds.

#include "EEPROM.h"

void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

const int patternAddress = 0;
const int currentLEDAddress = 1;
const uint8_t btnPin = 12;
const uint8_t leds[] = {2, 3, 4, 5, 6, 7, 8, 9, 10};
const uint8_t leds_size = sizeof(leds) / sizeof(uint8_t);
const uint8_t ledsLastIndex = leds_size - 1;
const unsigned long clickDebounceDelay = 10;
unsigned long milisecs = 0;
unsigned long btnClickTime = 0;
bool btnPreviousValue = 0, btnCurrentState = 0, justClicked = false;

void checkIfButtonPressed() {
    justClicked = false;
    bool btnCurrentValue = digitalRead(btnPin);
    if (btnCurrentValue != btnPreviousValue)
        btnClickTime = millis();
    if ((millis() - btnClickTime) > clickDebounceDelay && btnCurrentValue != btnCurrentState) {
        btnCurrentState = btnCurrentValue;
        justClicked = btnCurrentState;
    }
    btnPreviousValue = btnCurrentValue;
}

struct LED_Pattern {
    uint8_t currentLED = 0;

    virtual void setup();

    virtual void action();

    virtual void termination();
};

struct PatternOneDirection : LED_Pattern {
    void setup() override {
        digitalWrite(leds[currentLED], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < 150)
            return;
        milisecs = millis();
        digitalWrite(leds[currentLED++], LOW);
        if (currentLED > ledsLastIndex)
            currentLED = 0;
        EEPROM.write(currentLEDAddress, currentLED);
        digitalWrite(leds[currentLED], HIGH);
    }

    void termination() override {
        digitalWrite(leds[currentLED], LOW);
        currentLED = 0;
    }
};

struct PatternOneDirection_Inversed : LED_Pattern {
    void setup() override {
        digitalWrite(leds[currentLED], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < 150)
            return;
        milisecs = millis();
        digitalWrite(leds[currentLED--], LOW);
        if (currentLED > ledsLastIndex)
            currentLED = ledsLastIndex;
        EEPROM.write(currentLEDAddress, currentLED);
        digitalWrite(leds[currentLED], HIGH);
    }

    void termination() override {
        digitalWrite(leds[currentLED], LOW);
        currentLED = ledsLastIndex;
    }
};

struct PatternBackAndForth : LED_Pattern {
    bool isInverting = false;

    void setup() override {
        digitalWrite(leds[currentLED], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < 150)
            return;
        milisecs = millis();
        digitalWrite(leds[currentLED], LOW);
        if (currentLED == 0)
            isInverting = false;
        if (currentLED == ledsLastIndex)
            isInverting = true;
        currentLED += isInverting ? -1 : 1;
        EEPROM.write(currentLEDAddress, currentLED);
        digitalWrite(leds[currentLED], HIGH);
    }

    void termination() override {
        digitalWrite(leds[currentLED], LOW);
        currentLED = 0;
        isInverting = false;
    }
};

struct PatternOnceEachSide : LED_Pattern {
    bool otherSide = true;

    void setup() override {
        digitalWrite(leds[currentLED], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < 200)
            return;
        milisecs = millis();
        digitalWrite(leds[otherSide ? currentLED : ledsLastIndex - currentLED], LOW);
        if (otherSide) {
            EEPROM.write(currentLEDAddress, ledsLastIndex - currentLED);
            digitalWrite(leds[ledsLastIndex - currentLED], HIGH);
            otherSide = !otherSide;
            return;
        }
        if (++currentLED == leds_size)
            currentLED = 0;
        EEPROM.write(currentLEDAddress, currentLED);
        digitalWrite(leds[currentLED], HIGH);
        otherSide = !otherSide;
    }

    void termination() override {
        for (int i = currentLED; i < leds_size; i += 2)
            digitalWrite(leds[i], LOW);
        currentLED = 0;
        otherSide = true;
    }
};

struct PatternMeetCenter : LED_Pattern {
    void setup() override {
        if (currentLED > ledsLastIndex / 2)
            currentLED = currentLED / 2;
        digitalWrite(leds[currentLED], HIGH);
        digitalWrite(leds[ledsLastIndex - currentLED], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < 200)
            return;
        milisecs = millis();
        digitalWrite(leds[currentLED], LOW);
        digitalWrite(leds[ledsLastIndex - currentLED], LOW);
        ++currentLED;
        if (currentLED > ledsLastIndex - currentLED)
            currentLED = 0;
        EEPROM.write(currentLEDAddress, currentLED);
        digitalWrite(leds[currentLED], HIGH);
        digitalWrite(leds[ledsLastIndex - currentLED], HIGH);
    }

    void termination() override {
        digitalWrite(leds[currentLED], LOW);
        digitalWrite(leds[ledsLastIndex - currentLED], LOW);
        currentLED = 0;
    }
};

struct PatternMeetCenter_Continuous : LED_Pattern {
    void setup() override {
        digitalWrite(leds[currentLED], HIGH);
        digitalWrite(leds[ledsLastIndex - currentLED], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < 200)
            return;
        milisecs = millis();
        digitalWrite(leds[currentLED], LOW);
        digitalWrite(leds[ledsLastIndex - currentLED], LOW);
        ++currentLED;
        if (currentLED > ledsLastIndex)
            currentLED = 1;
        EEPROM.write(currentLEDAddress, currentLED);
        digitalWrite(leds[currentLED], HIGH);
        digitalWrite(leds[ledsLastIndex - currentLED], HIGH);
    }

    void termination() override {
        digitalWrite(leds[currentLED], LOW);
        digitalWrite(leds[ledsLastIndex - currentLED], LOW);
        currentLED = 0;
    }
};

struct PatternEveryOther : LED_Pattern {
    void setup() override {
        currentLED = currentLED % 2 == 1;
        for (int i = currentLED; i < leds_size; i += 2)
            digitalWrite(leds[i], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < 300)
            return;
        milisecs = millis();
        for (int i = currentLED; i < leds_size; i += 2)
            digitalWrite(leds[i], LOW);
        currentLED = !currentLED;
        EEPROM.write(currentLEDAddress, currentLED);
        for (int i = currentLED; i < leds_size; i += 2)
            digitalWrite(leds[i], HIGH);
    }

    void termination() override {
        for (int i = currentLED; i < leds_size; i += 2)
            digitalWrite(leds[i], LOW);
        currentLED = 0;
    }
};

struct PatternEachUntilAllLitThenOpposite : LED_Pattern {
    bool allLit = false;

    void setup() override {
        currentLED = 0;
        digitalWrite(leds[currentLED], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < 150)
            return;
        milisecs = millis();
        if (allLit) {
            digitalWrite(leds[currentLED], LOW);
            if (++currentLED == leds_size) {
                allLit = !allLit;
                currentLED = 0;
                return;
            }
        } else {
            digitalWrite(leds[currentLED], HIGH);
            if (++currentLED == leds_size) {
                allLit = !allLit;
                currentLED = 0;
                return;
            }
        }
        EEPROM.write(currentLEDAddress, currentLED);
    }

    void termination() override {
        for (int i = 0; i < leds_size; ++i)
            digitalWrite(leds[i], LOW);
        currentLED = 0;
        allLit = false;
    }
};

struct PatternEachThenBlinkAll : LED_Pattern {
    static const uint8_t maxBlinks = 4;
    bool allLit = false;
    uint8_t counter = 0;
    uint16_t actionDelay = 150;

    void setup() override {
        currentLED = 0;
        digitalWrite(leds[currentLED], HIGH);
        milisecs = millis();
    }

    void action() override {
        if (millis() - milisecs < actionDelay)
            return;
        milisecs = millis();
        if (allLit || counter != 0) {
            if (counter == maxBlinks) {
                allLit = false;
                counter = 0;
                actionDelay = 150;
                return;
            }
            for (int i = 0; i < leds_size; ++i)
                digitalWrite(leds[i], !allLit);
            if (allLit)
                ++counter;
            allLit = !allLit;
            if (counter == maxBlinks)
                actionDelay = 200;
        } else {
            digitalWrite(leds[currentLED], HIGH);
            if (++currentLED == leds_size) {
                allLit = true;
                currentLED = 0;
                actionDelay = 500;
                return;
            }
            EEPROM.write(currentLEDAddress, currentLED);
        }
    }

    void termination() override {
        for (int i = 0; i < leds_size; ++i)
            digitalWrite(leds[i], LOW);
        currentLED = 0;
        allLit = false;
        counter = 0;
        actionDelay = 150;
    }
};

PatternOneDirection oneDir;
PatternOneDirection_Inversed oneDir_Inv;
PatternBackAndForth backAndForth;
PatternOnceEachSide onceEachSide;
PatternMeetCenter meetCenter;
PatternMeetCenter_Continuous meetCenter_Cont;
PatternEveryOther everyOther;
PatternEachUntilAllLitThenOpposite eachUntilAllLit;
PatternEachThenBlinkAll eachThenBlinkAll;

LED_Pattern *patternPtr;
LED_Pattern *patterns[] = {&oneDir, &oneDir_Inv, &backAndForth, &onceEachSide, &meetCenter, &meetCenter_Cont, &everyOther, &eachUntilAllLit, &eachThenBlinkAll};
const uint8_t patterns_size = sizeof(patterns) / sizeof(LED_Pattern *);
uint8_t currentPattern = 0;

void setup() {
    disableBuiltinLED();
    pinMode(btnPin, INPUT);
    for (uint8_t i = 0; i < leds_size; ++i)
        pinMode(leds[i], OUTPUT);
    currentPattern = EEPROM.read(patternAddress);
    if (currentPattern >= patterns_size)
        currentPattern = 0;
    patternPtr = patterns[currentPattern];
    patternPtr->currentLED = EEPROM.read(currentLEDAddress);
    if (patternPtr->currentLED > ledsLastIndex)
        patternPtr->currentLED = 0;
    patternPtr->setup();
}

void loop() {
    checkIfButtonPressed();
    if (justClicked) {
        uint8_t currentLED = patternPtr->currentLED;
        patternPtr->termination();
        if (++currentPattern == patterns_size)
            currentPattern = 0;
        patternPtr = patterns[currentPattern];
        patternPtr->currentLED = currentLED;
        patternPtr->setup();
        EEPROM.write(patternAddress, currentPattern);
    }
    patternPtr->action();
}
