//requirements: 1 button, 1 seven segment display, 1 buzzer and optionally 1 LED

//this code is not readable or maintainable on purpose, and was written solely for learning purposes.

//Change to 0 if using a CC (Common Cathode) display
#define anodeDisplay 1

//Segment pins: from A to DP: 2, 3, 4, 5, 6, 7, 8, 9
//Buzzer pin: 10
//LED pin: 11
//Button pin: 12

void clearDisplay() {
#if anodeDisplay
    PORTD = PORTD | 0b11111100;
    PORTB = PORTB | 0b00000011;
#else
    PORTD = PORTD & 0b00000011;
    PORTB = PORTB & 0b11111100;
#endif
}

void displayZero() {
#if anodeDisplay
    PORTD = PORTD & 0b00000011;
    PORTB = PORTB | 0b00000011;
#else
    PORTD = PORTD | 0b11111100;
    PORTB = PORTB & 0b11111100;
#endif
}

void displayOne() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b11100111;
    PORTB = PORTB | 0b00000011;
#else
    PORTD = (PORTD & 0b00000011) | 0b00011000;
    PORTB = PORTB & 0b11111100;
#endif
}

void displayTwo() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b10010011;
    PORTB = (PORTB | 0b00000011) & 0b11111110;
#else
    PORTD = (PORTD & 0b00000011) | 0b01101100;
    PORTB = (PORTB & 0b11111100) | 0b00000001;
#endif
}

void displayThree() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b11000011;
    PORTB = (PORTB | 0b00000011) & 0b11111110;
#else
    PORTD = (PORTD & 0b00000011) | 0b00111100;
    PORTB = (PORTB & 0b11111100) | 0b00000001;
#endif
}

void displayFour() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b01100111;
    PORTB = (PORTB | 0b00000011) & 0b11111110;
#else
    PORTD = (PORTD & 0b00000011) | 0b10011000;
    PORTB = (PORTB & 0b11111100) | 0b00000001;
#endif
}

void displayFive() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b01001011;
    PORTB = (PORTB | 0b00000011) & 0b11111110;
#else
    PORTD = (PORTD & 0b00000011) | 0b10110100;
    PORTB = (PORTB & 0b11111100) | 0b00000001;
#endif
}

void displaySix() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b00001011;
    PORTB = (PORTB | 0b00000011) & 0b11111110;
#else
    PORTD = (PORTD & 0b00000011) | 0b11110100;
    PORTB = (PORTB & 0b11111100) | 0b00000001;
#endif
}

void displaySeven() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b11100011;
    PORTB = PORTB | 0b00000011;
#else
    PORTD = (PORTD & 0b00000011) | 0b00011100;
    PORTB = PORTB & 0b11111100;
#endif
}

void displayEight() {
#if anodeDisplay
    PORTD = PORTD & 0b00000011;
    PORTB = (PORTB | 0b00000011) & 0b11111110;
#else
    PORTD = PORTD | 0b11111100;
    PORTB = (PORTB & 0b11111100) | 0b00000001;
#endif
}

void displayNine() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b01100011;
    PORTB = (PORTB | 0b00000011) & 0b11111110;
#else
    PORTD = (PORTD & 0b00000011) | 0b10011100;
    PORTB = (PORTB & 0b11111100) | 0b00000001;
#endif
}

void displayF() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b00111011;
    PORTB = (PORTB | 0b00000011) & 0b11111110;
#else
    PORTD = (PORTD & 0b00000011) | 0b11000100;
    PORTB = (PORTB & 0b11111100) | 0b00000001;
#endif
}

void displayU() {
#if anodeDisplay
    PORTD = (PORTD | 0b11111100) & 0b00000111;
    PORTB = PORTB | 0b00000011;
#else
    PORTD = (PORTD & 0b00000011) | 0b11111000;
    PORTB = PORTB & 0b11111100;
#endif
}

void (*const displayNumberFunctions[])(void) = {displayZero, displayOne, displayTwo, displayThree, displayFour, displayFive, displaySix, displaySeven, displayEight, displayNine};

const unsigned long clickDebounceDelay = 20;
unsigned long btnClickTime = 0;
bool btnPreviousValue = 0, btnCurrentState = 0, justClicked = false;

void checkIfButtonPressed() {
    justClicked - false;
    bool btnCurrentValue = PINB & 0b00010000;
    if (btnCurrentValue != btnPreviousValue)
        btnClickTime = millis();
    if ((millis() - btnClickTime) > clickDebounceDelay && btnCurrentValue != btnCurrentState) {
        btnCurrentState = btnCurrentValue;
        justClicked = btnCurrentState;
    }
    btnPreviousValue = btnCurrentValue;
}

bool F_or_U = true, pauseCD = false, stopAlarm = false, alarmRinging = false, ringAlarm = false;
int counter = 9;
unsigned long milis = 0, milisAlarm = 0, difference = 0;

void setup() {
    DDRD |= 0b11111100; //setting pins 2 to 7 as OUTPUT
    DDRB = (DDRB | 0b00101111) & 0b11101111; //setting pins 8, 9, 10, 11 and 13 as OUTPUT and 12 as INPUT
    PORTB &= 0b11011111; //disabling LED_BUILTIN
    TCCR1A |= _BV(COM1B1); //sets pin 10 to PWM output. check wiring.analog.c for more info
    OCR1B = 0; //pin 10 (buzzer) to 0 PWM (LOW)
    displayNine();
}

void loop() {
    checkIfButtonPressed();
    if (counter > 1 && counter < 10) {
        if (justClicked) {
            pauseCD = !pauseCD;
            if (pauseCD)
                difference = millis() - milis;
            else
                milis = millis() - difference;
        }
        if (pauseCD)
            return;
        if (millis() - milis > 1000) {
            milis = millis();
            (*displayNumberFunctions[--counter])();
        }
        return;
    }
    if (justClicked) {
        stopAlarm = !stopAlarm;
        milis = 0;
        alarmRinging = false;
        ringAlarm = false;
        PORTB = PORTB & 0b11110111; //disable LED
        OCR1B = 0; //disable buzzer
    }
    if (stopAlarm)
        return;
    if (millis() - milis > 1000) {
        milis = millis();
        if (F_or_U)
            displayF();
        else
            displayU();
        F_or_U = !F_or_U;
        milisAlarm = milis;
        alarmRinging = true;
        PORTB = PORTB | 0b00001000; //enable LED
        OCR1B = 255; //set buzzer to max
    }
    if (alarmRinging) {
        if (millis() - milis > 500) {
            alarmRinging = false;
            ringAlarm = false;
            PORTB = PORTB & 0b11110111; //disable LED
            OCR1B = 0; //disable buzzer
        }
        if (millis() - milisAlarm > 100) {
            milisAlarm = millis();
            if (ringAlarm) {
                PORTB = PORTB | 0b00001000; //enable LED
                OCR1B = 255; //set buzzer to max
            } else {
                PORTB = PORTB & 0b11110111; //disable LED
                OCR1B = 0; //disable buzzer
            }
            ringAlarm = !ringAlarm;
        }
    }
}
