//requirements: 1 or more RGB LEDs in parallel connected to PWM

void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

#define UINT8_MAX 0xFF

const uint8_t redPin = 9, greenPin = 6, bluePin = 3;
const unsigned long colorCycle = 5000;

struct Color {
    uint8_t r = 0, g = 0, b = 0;

    Color() = default;

    Color(uint8_t r, uint8_t g, uint8_t b) : r(r), g(g), b(b) {}

    void outputToPins() {
        Color brightnessNormalizedColor = normalized();
        analogWrite(redPin, brightnessNormalizedColor.r);
        analogWrite(greenPin, brightnessNormalizedColor.g);
        analogWrite(bluePin, brightnessNormalizedColor.b);
    }

    Color normalized() {
        float highest = max(max(r, g), b);
        float fr = r / highest, fg = g / highest, fb = b / highest;
        return Color{UINT8_MAX * fr, UINT8_MAX * fg, UINT8_MAX * fb};
    }

    Color &Interpolate(const Color &color, float interpolation) {
        this->r += (color.r - this->r) * interpolation;
        this->g += (color.g - this->g) * interpolation;
        this->b += (color.b - this->b) * interpolation;
        return *this;
    }

    static Color Interpolate(Color lcolor, const Color &rcolor, float interpolation) {
        lcolor.r += (rcolor.r - lcolor.r) * interpolation;
        lcolor.g += (rcolor.g - lcolor.g) * interpolation;
        lcolor.b += (rcolor.b - lcolor.b) * interpolation;
        return lcolor;
    }

    Color &operator+=(const Color &color) {
        this->r += color.r;
        this->g += color.g;
        this->b += color.b;
        return *this;
    }

    Color &operator-=(const Color &color) {
        this->r -= color.r;
        this->g -= color.g;
        this->b -= color.b;
        return *this;
    }

    Color operator+(const Color &color) {
        return Color(this->r + color.r, this->g + color.g, this->b + color.b);
    }

    Color operator-(const Color &color) {
        return Color(this->r - color.r, this->g - color.g, this->b - color.b);
    }

    friend Color operator+(Color lcolor, const Color &rcolor) {
        lcolor += rcolor;
        return lcolor;
    }

    friend Color operator-(Color lcolor, const Color &rcolor) {
        lcolor -= rcolor;
        return lcolor;
    }

    Color &operator*=(const int val) {
        this->r *= val;
        this->g *= val;
        this->b *= val;
        return *this;
    }

    Color &operator/=(const int val) {
        this->r /= val;
        this->g /= val;
        this->b /= val;
        return *this;
    }
};

const Color RED = {UINT8_MAX, 0, 0}, GREEN = {0, UINT8_MAX, 0}, BLUE = {0, 0, UINT8_MAX};
const Color colors[] = {RED, GREEN, BLUE};
const uint8_t colors_size = sizeof(colors) / sizeof(Color);

Color currentColor = colors[0];
uint8_t nextColor = 1;
unsigned long milis = 0;

void setup() {
    disableBuiltinLED();
    pinMode(redPin, OUTPUT);
    pinMode(greenPin, OUTPUT);
    pinMode(bluePin, OUTPUT);
    currentColor.outputToPins();
}

void loop() {
    unsigned long millisDiff;
    milis = millis();
    while ((millisDiff = millis() - milis) <= colorCycle)
        Color::Interpolate(currentColor, colors[nextColor], millisDiff / (float) colorCycle).outputToPins();
    currentColor = colors[nextColor];
    if (++nextColor == colors_size)
        nextColor = 0;
}
