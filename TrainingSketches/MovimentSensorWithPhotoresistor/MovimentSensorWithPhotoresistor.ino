//requirments: 1 photoresistor, at least 1 10k resistor, 1 buzzer

void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

const uint8_t buzzerPin = 3;
const uint8_t analogInputPin = A0;
//Values will be divided by tolerance then added to/subtracted from previous readings. I.e. 20 means 5%, 5 means 20%, 2 means 50%, etc.
//This is to avoid using floats
const int tolerance = 10;
const int delayBetweenReads = 25;
const int buffer_size = 4;
int buffer[buffer_size] = {0};
int bIndex = 0;

void soundAlarm() {
    for (int i = 0; i < 3; ++i) {
        analogWrite(buzzerPin, 255);
        delay(100);
        analogWrite(buzzerPin, 0);
        delay(100);
    }
}

void setBufferToValue(int value) {
    for (int i = 0; i < buffer_size; ++i)
        buffer[i] = value;
}

void setup() {
    disableBuiltinLED();
    pinMode(buzzerPin, OUTPUT);
    pinMode(analogInputPin, INPUT);
    setBufferToValue(analogRead(analogInputPin));
}

void loop() {
    delay(delayBetweenReads);
    int currReading = analogRead(analogInputPin);
    for (int i = 0; i < buffer_size; ++i) {
        int variation = buffer[i] / tolerance;
        int toleranceLow = buffer[i] - variation;
        int toleranceHigh = buffer[i] + variation;
        if (currReading < toleranceLow || currReading > toleranceHigh) {
            soundAlarm();
            setBufferToValue(currReading);
            return;
        }
    }
    if (++bIndex == buffer_size)
        bIndex = 0;
    buffer[bIndex] = currReading;
}
