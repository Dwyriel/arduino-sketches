//requirements: multiple buttons, 1 seven segment display (or bigger)

void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

//Change to 0 if using a CC (Common Cathode) display
#define anodeDisplay 1
#if anodeDisplay
#define START_STATE LOW
#else
#define START_STATE HIGH
#endif
#undef anodeDisplay

const unsigned long clickDebounceDelay = 10;
const uint8_t segments[] = {A0, A1, A2, A3, A4, A5, 2}; //pins
const uint8_t segments_size = 7;
const uint8_t segA = 0;//placement of segment within 'segments' array
const uint8_t segB = 1;
const uint8_t segC = 2;
const uint8_t segD = 3;
const uint8_t segE = 4;
const uint8_t segF = 5;
const uint8_t segG = 6;
bool segmentsState[segments_size];

void setLEDsToCurrentState(){
    for(uint8_t i = 0; i < segments_size; ++i)
        digitalWrite(segments[i], segmentsState[i]);
}

void invertLEDState(uint8_t index){
    segmentsState[index] = !segmentsState[index];
}

struct Button {
    void (* action)(void);
    uint8_t pin;
    bool justClicked = 0;

    explicit Button(uint8_t pin, void (* action)(void)) : pin(pin), action(action) {};

private:
    unsigned long clickTime = 0;
    bool previousValue = 0xFF;
    bool currentState = 0;

public:
    void checkIfWasPressed() {
        justClicked = false;
        bool currentValue = digitalRead(this->pin);
        if (currentValue != this->previousValue)
            this->clickTime = millis();
        if ((millis() - this->clickTime) > clickDebounceDelay && currentValue != this->currentState){
            this->currentState = currentValue;
            justClicked = this->currentState;
        }
        this->previousValue = currentValue;
    }
};

void action1(){
    invertLEDState(segA);
    invertLEDState(segB);
    invertLEDState(segG);
}

void action2(){
    invertLEDState(segG);
    invertLEDState(segE);
    invertLEDState(segD);
}

void action3(){
    invertLEDState(segB);
    invertLEDState(segG);
    invertLEDState(segE);
}

void action4(){
    invertLEDState(segF);
    invertLEDState(segG);
    invertLEDState(segC);
}

Button btns[] = {Button(12, &action1), Button(11, &action2), Button(10, &action3), Button(9, &action4)};
const uint8_t btns_size = sizeof(btns) / sizeof(Button);

void setup() {
    disableBuiltinLED();
    for (uint8_t i = 0; i < btns_size; ++i)
        pinMode(btns[i].pin, INPUT);
    for (uint8_t i = 0; i < segments_size; ++i){
        pinMode(segments[i], OUTPUT);
        segmentsState[i] = START_STATE;
    }
    setLEDsToCurrentState();
}

void loop() {
    for (uint8_t i = 0; i < btns_size; ++i){
        btns[i].checkIfWasPressed();
        if (btns[i].justClicked){
            btns[i].action();
            setLEDsToCurrentState();
        }
    }
}
