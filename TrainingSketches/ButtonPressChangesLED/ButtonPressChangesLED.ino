//requirements: push button and a few LEDs
#include "EEPROM.h"

void disableBuiltinLED() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

const uint8_t buttonPin = 12;
const uint8_t leds[] = {2, 3, 4, 5};
const uint8_t leds_size = sizeof(leds) / sizeof(uint8_t);
const unsigned long clickDebounceDelay = 10;
const int ledEepromAddress = 0;

uint8_t currentLED = 0;
bool buttonPreviousValue = 0xFF, buttonCurrentState = 0, justClicked = false;
unsigned long clickTime = 0;

void changeLEDs() {
    digitalWrite(leds[currentLED++], LOW);
    if (currentLED == leds_size)
        currentLED = 0;
    EEPROM.write(ledEepromAddress, currentLED);
    digitalWrite(leds[currentLED], HIGH);
}

void checkIfPressed() {
    justClicked = false;
    bool buttonCurrentValue = digitalRead(buttonPin);
    if (buttonCurrentValue && buttonCurrentValue != buttonPreviousValue)
        clickTime = millis();
    if ((millis() - clickTime) > clickDebounceDelay && buttonCurrentValue != buttonCurrentState) {
        buttonCurrentState = buttonCurrentValue;
        justClicked = buttonCurrentState;
    }
    buttonPreviousValue = buttonCurrentValue;
}

void setup() {
    disableBuiltinLED();
    pinMode(buttonPin, INPUT);
    for (int i = 0; i < leds_size; ++i)
        pinMode(leds[i], OUTPUT);
    currentLED = EEPROM.read(ledEepromAddress);
    if (currentLED >= leds_size) {
        currentLED = 0;
        EEPROM.write(ledEepromAddress, currentLED);
    }
    digitalWrite(leds[currentLED], HIGH);
}

void loop() {
    checkIfPressed();
    if (justClicked)
        changeLEDs();
}
